const Proveedores = require("../models/Proveedores");
// CONSULTAR PROOVEDOR POR JSON
exports.consultarProveedores = async(req,res)=>{

    try{
        const proveedores=await Proveedores.find();
        res.json(proveedores)
    }catch(error){
        console.log(error);
        res.status(500).send('hubo un error al consultar los datos');
    }
}
// AGREGAR PROOVEDOR  POR JSON
exports.agregarProveedores=async(req,res) => {
    try{
        let proveedor;
        proveedor=new Proveedores(req.body);

        await proveedor.save();
        res.send(proveedor);

    }catch (error) {
        console.log(error);
        res.status(500).send("no se puede guardar el producto");

    }
}
// OBTENER PROOVEDOR  POR ID DESDE EL JSON
exports.obtenerProveedores = async (req,res) => {
    try{
        let proveedor=await Proveedores.findById(req.params.id);

        if (!proveedor){
            res.status(404).json({msg:'no se puede encontrar ese proveedor'})
        }
        res.send(proveedor);

    }catch (error) {
        console.log(error);
        res.status(500).send("no hay conexion BD");

    }
}
// Eliminar PROOVEDOR 

exports.eliminarProveedores = async (req,res) =>{
    try{
        let proveedor=await Proveedores.findById(req.params.id);

        if (!proveedor){
            res.status(404).json({msg:'no existe proveedor'})
        }
        await Proveedores.findOneAndRemove({_id: req.params.id });
        res.json({msg: 'el proveedor está eliminado'});

    }catch (error) {
        console.log(error);
        res.status(500).send("no hay conexion BD");

    }
}
// ACTUALIZAR PROOVEDOR 
exports.actualizarProveedores = async (req,res) => {
    try{
        const {nombre, tipo, dia_pedido, valor_minimo_pedido, telefono, ciudad, correo} =req.body;
        let proveedor = await Proveedores.findById(req.params.id);
        if (!proveedor){
            res.status(404).json({msg: 'el proveedor no existe'});

        }
        proveedor.nombre= nombre;
        proveedor.tipo = tipo;
        proveedor.dia_pedido = dia_pedido;
        proveedor.valor_minimo_pedido = valor_minimo_pedido;
        proveedor.telefono = telefono;
        proveedor.ciudad = ciudad;
        proveedor.correo = correo;
        proveedor = await Proveedores.findOneAndUpdate({_id: req.params.id}, proveedor,{new:true});
        res.json(proveedor);
    }catch (error) {
        console.log(error);
        res.status(500).send("no hay conexion BD");
    }
}