const mongoose=require('mongoose');
//MODELO QUE DEBE SER IGUAL AL ESQUEMA DE LA BD
const clientesShema= mongoose.Schema({
    nombre:{
        type:String,
        required:true
    },
    identificacion:{
        type:Number,
        required:true
    },
    ciudad:{
        type:String,
        required:true
    },
    correo:{
        type:String,
        required:true
    },
    telefono:{
        type:Number,
        required:true
    }
    

},{ versionKey: false });

module.exports=mongoose.model("clientes", clientesShema);