import logo from './logo.svg';
import './App.css';
//IMPORTAR COMPONENTES
import CompoMProducto from './tienda/MostrarProductos';
import CompCrearProductos from './tienda/CrearProducto';
import CompEditProductos from './tienda/EditarProductos';
import CarritoCompras from './tienda/CarritoCompras';
//COMPONENTES MODULO PROVEEDORES
import CompCrearProveedor from './tienda/Modulo_Proveedores/CrearProveedor';
import CompoMProveedor from './tienda/Modulo_Proveedores/MostrarProveedor';
import CompEditProveedor from './tienda/Modulo_Proveedores/EditarProveedor'
//COMPONENTES MODULO CLIENTES
import CompCrearCliente from './tienda/Modulo_Clientes/CrearCliente';
import CompEditCliente from './tienda/Modulo_Clientes/Editar_cliente';
import CompoMCliente from './tienda/Modulo_Clientes/MostrarCliente';

//



//IMPORTAMOSLAS RUTAS
import { BrowserRouter, Route, Routes } from 'react-router-dom';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        
      </header>

<BrowserRouter>
  <Routes>

<Route path='/' element={<CompoMProducto />}></Route>
<Route path='/agregar' element={<CompCrearProductos/>}></Route>
<Route path='/editar/:id' element={<CompEditProductos/>}></Route>
<Route path='/carrito/' element={<CarritoCompras/>}></Route>



<Route path='/proveedores' element={<CompoMProveedor />}></Route>
<Route path='/agregarproveedor' element={<CompCrearProveedor/>}></Route>
<Route path='/editarproveedor/:id' element={<CompEditProveedor/>}></Route>



<Route path='/clientes' element={<CompoMCliente />}></Route>
<Route path='/agregarcliente' element={<CompCrearCliente/>}></Route>
<Route path='/editarcliente/:id' element={<CompEditCliente/>}></Route>






</Routes>


</BrowserRouter>
    </div>
  );
}

export default App;
