import { TYPES } from "../actions/CarritoAcciones"



export const CarritoEstadoInicial={
    product:[
        {id:1, nombre:"Nucitas", precio: 3200},
        {id:2, nombre:"Wafer", precio: 5200},
        {id:3, nombre:"Bom bom bum", precio: 8800},
        {id:4, nombre:"Conservitas", precio: 9000},
        {id:5, nombre:"Trululu", precio: 3400},
        {id:6, nombre:"Trocipollo", precio: 6200},
        {id:7, nombre:"Sparkies", precio: 23000},
        {id:8, nombre:"Quipito", precio: 3300},
        {id:9, nombre:"Gol", precio: 9200},
    ],
    carrito:[],
}

export function CarritoReducer(state,action){
    switch(action.type){
        case TYPES.AGREGA_RCARRITO:{let newItem = state.product.find((product) => product.id === action.payload);
            //console.log(newItem);
            let itemInCart = state.carrito.find((item) => item.id === newItem.id);

      return itemInCart
        ? {
            ...state,
            carrito: state.carrito.map((item) =>
              item.id === newItem.id
                ? { ...item, quantity: item.quantity + 1 }
                : item
            ),
          }
        : {
            ...state,
            carrito: [...state.carrito, { ...newItem, quantity: 1 }],
          };
    
        };
        case TYPES.REMOVER_UNO_CARRITO:{
            let itemToDelete = state.cart.find((item) => item.id === action.payload);

            return itemToDelete.quantity > 1
            ? {
                ...state,
                carrito: state.carrito.map((item)=>
                    item.id === action.payload
                    ? { ...item, quantity: item.quantity - 1 }
                    : item
                ),
                }
            : {
                ...state,
                carrito: state.carrito.filter((item) => item.id !== action.payload),
            };
        }
        case TYPES.REMOVER_TODO_CARRITO:{
                return{...state,
                carrito: state.carrito.filter((item) => item.id !== action.payload),
            };
        }
        case TYPES.LIMPIAR_CARRITO:
            return CarritoEstadoInicial;
        default:
            return state;
        }
        
    }
