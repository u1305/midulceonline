
import { useReducer } from 'react';
import { TYPES } from '../actions/CarritoAcciones';
import {CarritoEstadoInicial, CarritoReducer} from "../reducer/reducer";
import Carritoitems from './CarritoItems';
import ProductosItem from '../tienda/ProductosItems';
const URL = 'https://u13-05.herokuapp.com/api/productos/'




const CarritoCompras = ()=>{
    const [state,dispatch]= useReducer(CarritoReducer, CarritoEstadoInicial);


    const{product, carrito} =  state;

    const addToCart = (id)=>{
        //console.log(id);
        dispatch({type:TYPES.AGREGA_RCARRITO, payload:id});
    };
    const delFromCart = (id,all = false)=>{
        console.log(id, all);
        if (all){
            dispatch({type:TYPES.REMOVER_TODO_CARRITO, payload:id});
        }else{
                dispatch({type:TYPES.REMOVER_TODO_CARRITO, payload:id});
            
        }
    };
    const clearCart = ()=>{
        dispatch({type:TYPES.LIMPIAR_CARRITO});
    };





return(
    <div>
        <br></br>
        <br></br>
        <h3> Productos</h3>
        <br></br>
        <br></br>
        <article className='box grid-responsive'>
            {product.map((product) => (
            <ProductosItem key={product.id} data={product} addToCart={addToCart}/>
            ))}
        </article>
        <br></br>
        <h3>Carrito</h3>
        <br></br>
        <article className='box'></article>
        <button  className='button' onClick={clearCart}> Limpiar Carrito</button>
        <br></br>
        <br></br>
        
        {
            carrito.map ((item, index)=> <Carritoitems key={index} data={item}delFromCart={delFromCart}/>)
        }

        <div>
            <article>
            <form>
                <h1> Formulario de Pago </h1>
                <form className='formulario' action="/my-handling-form-page" method="post">
                <div class="mb-3">
                    <label id='label' for="exampleFormControlInput1" class="form-label" >Nombre</label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Pepito Perez"/>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Correo</label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"/>
                </div>
                <div class="mb-3">
                    <h2   for="exampleFormControlTextarea1" class="form-label">Valor del pedido: </h2><br></br>
                    <button type="button" class="btn btn-secondary btn-lg">Realizar Pago</button>
                    
                </div>
            </form>
            </form>
            </article>
        </div>
        
    </div>
);

















}
export default CarritoCompras