
const Carritoitems = ({data, delFromCart})=>{
    let {id, nombre, precio, quantity}=data;
    return(
        <div style={{borderBottom: "thin solid gray"}}>
            
            <h4>{nombre}</h4>
            <h5>${precio}.00 x {quantity} = ${precio*quantity}.00</h5>
            <button className="button" onClick={()=>delFromCart(id)}> Eliminar  Uno</button>
            <button className="button" onClick={()=>delFromCart(id, true)}> Eliminar Todos</button>
            <br></br>
            <br></br>
            
        </div>
    );
};

export default Carritoitems;