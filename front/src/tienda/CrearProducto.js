import axios from 'axios';
import {useState } from 'react';
import {useNavigate} from 'react-router-dom';


const URL = 'https://u13-05.herokuapp.com/api/productos/'

const CompCrearProductos  = () => {
    const [nombre, setNombre] = useState('');
    const [categoria, setCategoria] = useState ('');
    const [precio, setPrecio] = useState ('');
    const [presentacion, setPresentacion] = useState ('');
    const [cantidad, setCantidad] = useState ('');
    const [precio_venta, setPrecio_venta] = useState ('');
    const navigate = useNavigate();
//FUNCION GUARDAR
const save = async(g) => {
    g.preventDefault()
    await axios.post(URL,{nombre: nombre, categoria: categoria, precio: precio, presentacion: presentacion, cantidad: cantidad, precio_venta: precio_venta})
    navigate('/');
}
return(


    <div>
        <h2> Agregar Productos</h2>
        <form  onSubmit={save}>
    <div className='mb-3'>
        <label className=' form-label'>Nombre del Producto</label>
        <input value = {nombre} onChange = {(g) => setNombre (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Categoria</label>
        <input value = {categoria} onChange = {(g) => setCategoria (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Precio</label>
        <input value = {precio} onChange = {(g) => setPrecio (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Presentación</label>
        <input value = {presentacion} onChange = {(g) => setPresentacion (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Cantidad</label>
        <input value = {cantidad} onChange = {(g) => setCantidad (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label '>Precio Venta</label>
        <input value = {precio_venta} onChange = {(g) => setPrecio_venta (g.target.value)} type='text' className='form-control'></input>
    </div>
    <button type='submit' className='btn btn-primary'>Guardar</button>
    
    </form>
    </div>

)
}





export default CompCrearProductos