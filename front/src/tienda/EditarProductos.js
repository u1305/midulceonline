import axios from 'axios';
import { useEffect, useState} from 'react';
import { useNavigate, useParams } from 'react-router-dom';
const URL = 'https://u13-05.herokuapp.com/api/productos/'


const CompEditProductos  = () => {
    const [nombre, setNombre] = useState('');
    const [categoria, setCategoria] = useState ('');
    const [precio, setPrecio] = useState ('');
    const [presentacion, setPresentacion] = useState ('');
    const [cantidad, setCantidad] = useState ('');
    const [precio_venta, setPrecio_venta] = useState ('');
    const navigate = useNavigate();
    const {id} = useParams()

    const update = async (g) =>{
        g.preventDefault()
        await axios.put(`${URL}${id}`,{nombre: nombre, categoria: categoria, precio: precio, presentacion: presentacion, cantidad: cantidad, precio_venta: precio_venta})
        navigate('/');

        } 
        useEffect(() => {
            getProductoById();
            // eslint-disable-next-line
        },[])

const getProductoById = async ()=>{
    const res =  await axios.get(`${URL}${id}`)
    setNombre(res.data.nombre)
    setCategoria(res.data.categoria)
    setPrecio(res.data.precio);
    setPresentacion(res.data.presentacion)
    setCantidad(res.data.cantidad)
    setPrecio_venta(res.data.precio_venta)

}
return(


    <div>
        <h2> Editar Productos</h2>
        <form  onSubmit={update}>
    <div className='mb-3'>
        <label className=' form-label'>Nombre del Producto</label>
        <input value = {nombre} onChange = {(g) => setNombre (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Categoria</label>
        <input value = {categoria} onChange = {(g) => setCategoria (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Precio</label>
        <input value = {precio} onChange = {(g) => setPrecio (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Presentación</label>
        <input value = {presentacion} onChange = {(g) => setPresentacion (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Cantidad</label>
        <input value = {cantidad} onChange = {(g) => setCantidad (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label '>Precio Venta</label>
        <input value = {precio_venta} onChange = {(g) => setPrecio_venta (g.target.value)} type='text' className='form-control'></input>
    </div>
    <button type='submit' className='btn btn-primary'>Guardar</button>
    
    </form>
    </div>

)

    }
    export default CompEditProductos