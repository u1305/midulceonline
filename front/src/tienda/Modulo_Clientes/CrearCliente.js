import axios from 'axios';
import {useState } from 'react';
import {useNavigate} from 'react-router-dom';


const URL = 'https://u13-05.herokuapp.com/api/clientes/'

const CompCrearCliente  = () => {
    const [nombre, setNombre] = useState('');
    const [identificacion, setIdentificacion] = useState ('');
    const [ciudad, setCiudad] = useState ('');
    const [correo, setCorreo] = useState ('');
    const [telefono, setTelefono] = useState ('');
    
    const navigate = useNavigate();
//FUNCION GUARDAR
const save = async(h) => {
    h.preventDefault()
    await axios.post(URL,{nombre: nombre, identificacion: identificacion, telefono: telefono, ciudad: ciudad, correo: correo})
    navigate('/');
}
return(


    <div>
        <h2> Agregar Clientes</h2>
        <form  onSubmit={save}>
    <div className='mb-3'>
        <label className=' form-label'>Nombre del Cliente</label>
        <input value = {nombre} onChange = {(h) => setNombre (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Identificación</label>
        <input value = {identificacion} onChange = {(h) => setIdentificacion (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Ciudad</label>
        <input value = {ciudad} onChange = {(h) => setCiudad (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Correo</label>
        <input value = {correo} onChange = {(h) => setCorreo (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>telefono</label>
        <input value = {telefono} onChange = {(h) => setTelefono (h.target.value)} type='text' className='form-control'></input>
    </div>
    
    <button type='submit' className='btn btn-primary'>Guardar</button>
    
    </form>
    </div>

)
}




export default CompCrearCliente