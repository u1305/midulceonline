import axios from 'axios';
import { useEffect, useState} from 'react';
import { useNavigate, useParams } from 'react-router-dom';
const URL = 'https://u13-05.herokuapp.com/api/clientes/'


const CompEditCliente  = () => {
    const [nombre, setNombre] = useState('');
    const [identificacion, setIdentificacion] = useState ('');
    const [ciudad, setCiudad] = useState ('');
    const [correo, setCorreo] = useState ('');
    const [telefono, setTelefono] = useState ('');
    
    
    const navigate = useNavigate();
    const {id} = useParams()

    const update = async (g) =>{
        g.preventDefault()
        await axios.put(`${URL}${id}`,{nombre: nombre, identificacion:identificacion, ciudad: ciudad, correo: correo, telefono: telefono, })
        navigate('/');

        } 
        useEffect(() => {
            getClienteById();
            // eslint-disable-next-line
        },[])

const getClienteById = async ()=>{
    const res =  await axios.get(`${URL}${id}`)
    setNombre(res.data.nombre)
    setIdentificacion(res.data.identificacion)
    setCiudad(res.data.ciudad)
    setCorreo(res.data.correo)
    setTelefono(res.data.telefono)
}
return(


    <div>
        <h2> Editar Datos de Clientes</h2>
        <form  onSubmit={update}>
    <div className='mb-3'>
        <label className=' form-label'>Nombre del Cliente </label>
        <input value = {nombre} onChange = {(g) => setNombre (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>identificacion</label>
        <input value = {identificacion} onChange = {(g) => setIdentificacion (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label '>Ciudad</label>
        <input value = {ciudad} onChange = {(g) => setCiudad (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label '>Correo</label>
        <input value = {correo} onChange = {(g) => setCorreo (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label '>Telefono</label>
        <input value = {telefono} onChange = {(g) => setTelefono (g.target.value)} type='text' className='form-control'></input>
    </div>
    <button type='submit' className='btn btn-primary'>Guardar</button>
    
    </form>
    </div>

)

    }
    export default CompEditCliente