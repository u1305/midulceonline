import axios from 'axios';
import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom'
const URL = 'https://u13-05.herokuapp.com/api/clientes/'





const CompoMCliente = () => {
    const [cliente,  setCliente] = useState([])
    useEffect(()=>{
    getCliente();

    },[])
//MOSTRAR LOS CLIENTES

const getCliente = async () => {
    const result = await axios.get(URL)
    setCliente(result.data)
}

// ELIMINAR CLIENTE
const deleteCliente = async (id) => {
    await axios.delete(`${URL}${id}`)
    getCliente();

}
return(
    <div className='container'>
        <div className="row">
            <div className="col">
                <Link to="/agregarcliente/" className="btn btn-success mt-2 mt.2"><i className="fa-solid fa-pen-to-square"></i> Agregar Nuevo Cliente</Link>
                <table className="table">
                    <thead className="tableTheadBg">
                        <tr>
                            <th>Nombre</th>
                            <th>Identificación</th>
                            <th>Ciudad</th>
                            <th>Correo</th>
                            <th>Telefono</th>
                        </tr>
                    </thead>
                    <tbody>
                        {cliente.map((cliente, index) => (
                            <tr key = {index}>
                            <td>{cliente.nombre}</td>
                            <td>{cliente.identificacion}</td>
                            <td>{cliente.ciudad}</td>
                            <td>{cliente.correo}</td>
                            <td>{cliente.telefono}</td>
                            <td>
                            <Link to ={`/editarcliente/${cliente._id}`} className="btn btn-success"><i className="fa-solid fa-pen-to-square"></i> Editar</Link>
                            <button onClick={()=>deleteCliente(cliente._id)}className='btn btn-danger'><i class="fa-solid fa-trash-can"></i> Eliminar</button>
                            </td>
                            </tr>
                        ))}
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
)







}
export default CompoMCliente;