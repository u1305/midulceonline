import axios from 'axios';
import {useState } from 'react';
import {useNavigate} from 'react-router-dom';


const URL = 'https://u13-05.herokuapp.com/api/proveedores/'

const CompCrearProveedor  = () => {
    const [nombre, setNombre] = useState('');
    const [tipo, setTipo] = useState ('');
    const [dia_pedido, setDia_pedido] = useState ('');
    const [valor_minimo_pedido, setValor_minimo_pedido] = useState ('');
    const [telefono, setTelefono] = useState ('');
    const [ciudad, setCiudad] = useState ('');
    const [correo, setCorreo] = useState ('');
    const navigate = useNavigate();
//FUNCION GUARDAR
const save = async(h) => {
    h.preventDefault()
    await axios.post(URL,{nombre: nombre, tipo: tipo, dia_pedido: dia_pedido, valor_minimo_pedido: valor_minimo_pedido,
    telefono: telefono, ciudad: ciudad, correo: correo})
    navigate('/');
}
return(


    <div>
        <h2> Agregar Proveedores</h2>
        <form  onSubmit={save}>
    <div className='mb-3'>
        <label className=' form-label'>Nombre del Proveedor</label>
        <input value = {nombre} onChange = {(h) => setNombre (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Tipo de Elemento</label>
        <input value = {tipo} onChange = {(h) => setTipo (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Dia Para Pedido</label>
        <input value = {dia_pedido} onChange = {(h) => setDia_pedido (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Valor minimo del pedido</label>
        <input value = {valor_minimo_pedido} onChange = {(h) => setValor_minimo_pedido (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>telefono</label>
        <input value = {telefono} onChange = {(h) => setTelefono (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label '>Ciudad</label>
        <input value = {ciudad} onChange = {(h) => setCiudad (h.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label '>Correo</label>
        <input value = {correo} onChange = {(h) => setCorreo (h.target.value)} type='text' className='form-control'></input>
    </div>
    <button type='submit' className='btn btn-primary'>Guardar</button>
    
    </form>
    </div>

)
}




export default CompCrearProveedor