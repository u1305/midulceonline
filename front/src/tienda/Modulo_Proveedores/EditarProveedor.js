import axios from 'axios';
import { useEffect, useState} from 'react';
import { useNavigate, useParams } from 'react-router-dom';
const URL = 'https://u13-05.herokuapp.com/api/proveedores/'


const CompEditProveedor  = () => {
    const [nombre, setNombre] = useState('');
    const [tipo, setTipo] = useState ('');
    const [dia_pedido, setDia_pedido] = useState ('');
    const [valor_minimo_pedido, setValor_minimo_pedido] = useState ('');
    const [telefono, setTelefono] = useState ('');
    const [ciudad, setCiudad] = useState ('');
    const [correo, setCorreo] = useState ('');
    const navigate = useNavigate();
    const {id} = useParams()

    const update = async (g) =>{
        g.preventDefault()
        await axios.put(`${URL}${id}`,{nombre: nombre, tipo: tipo, dia_pedido: dia_pedido,
        valor_minimo_pedido: valor_minimo_pedido, telefono: telefono, ciudad: ciudad,
    correo: correo})
        navigate('/');

        } 
        useEffect(() => {
            getProveedorById();
            // eslint-disable-next-line
        },[])

const getProveedorById = async ()=>{
    const res =  await axios.get(`${URL}${id}`)
    setNombre(res.data.nombre)
    setTipo(res.data.tipo)
    setDia_pedido(res.data.dia_pedido);
    setValor_minimo_pedido(res.data.valor_minimo_pedido)
    setTelefono(res.data.telefono)
    setCiudad(res.data.ciudad)
    setCorreo(res.data.correo)

}
return(


    <div>
        <h2> Editar Datos de Proveedor</h2>
        <form  onSubmit={update}>
    <div className='mb-3'>
        <label className=' form-label'>Nombre del proveedor </label>
        <input value = {nombre} onChange = {(g) => setNombre (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Tipo de proveedor</label>
        <input value = {tipo} onChange = {(g) => setTipo (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Dia del pedido</label>
        <input value = {dia_pedido} onChange = {(g) => setDia_pedido (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Valor minimo del pedido</label>
        <input value = {valor_minimo_pedido} onChange = {(g) => setValor_minimo_pedido (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label'>Telefono</label>
        <input value = {telefono} onChange = {(g) => setTelefono (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label '>Ciudad</label>
        <input value = {ciudad} onChange = {(g) => setCiudad (g.target.value)} type='text' className='form-control'></input>
    </div>
    <div className='mb-3'>
        <label className=' form-label '>Correo</label>
        <input value = {correo} onChange = {(g) => setCorreo (g.target.value)} type='text' className='form-control'></input>
    </div>
    <button type='submit' className='btn btn-primary'>Guardar</button>
    
    </form>
    </div>

)

    }
    export default CompEditProveedor