import axios from 'axios';
import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom'
const URL = 'https://u13-05.herokuapp.com/api/proveedores/'





const CompoMProveedor = () => {
    const [proveedor,  setProveedor] = useState([])
    useEffect(()=>{
    getProveedor();

    },[])
//MOSTRAR LOS PROVEEDORES

const getProveedor = async () => {
    const result = await axios.get(URL)
    setProveedor(result.data)
}

// ELIMINAR PROVEEDOR
const deleteProveedor = async (id) => {
    await axios.delete(`${URL}${id}`)
    getProveedor();

}
return(
    <div className='container'>
        <div className="row">
            <div className="col">
                <Link to="/agregarproveedor/" className="btn btn-success mt-2 mt.2"><i className="fa-solid fa-pen-to-square"></i> Agregar Nuevo Proveedor</Link>
                <table className="table">
                    <thead className="tableTheadBg">
                        <tr>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Dia de pedido</th>
                            <th>Valor minimo de Pedido</th>
                            <th>Telefono</th>
                            <th>Ciudad</th>
                            <th>Correo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {proveedor.map((proveedor, index) => (
                            <tr key = {index}>
                            <td>{proveedor.nombre}</td>
                            <td>{proveedor.tipo}</td>
                            <td>{proveedor.dia_pedido}</td>
                            <td>{proveedor.valor_minimo_pedido}</td>
                            <td>{proveedor.telefono}</td>
                            <td>{proveedor.ciudad}</td>
                            <td>{proveedor.correo}</td>
                            <td>
                            <Link to ={`/editarproveedor/${proveedor._id}`} className="btn btn-success"><i className="fa-solid fa-pen-to-square"></i> Editar</Link>
                            <button onClick={()=>deleteProveedor(proveedor._id)}className='btn btn-danger'><i class="fa-solid fa-trash-can"></i> Eliminar</button>
                            </td>
                            </tr>
                        ))}
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
)







}
export default CompoMProveedor;