import axios from 'axios';
import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom'
const URL = 'https://u13-05.herokuapp.com/api/productos/'





const CompoMProducto = () => {
    const [productos,  setProducto] = useState([])
    useEffect(()=>{
    getProductos();

    },[])
//MOSTRAR LOS PRODUCTOS

const getProductos = async () => {
    const result = await axios.get(URL)
    setProducto(result.data)
}

// ELIMINAR PRODUCTO
const deleteProducto = async (id) => {
    await axios.delete(`${URL}${id}`)
    getProductos();

}
return(
    <div className='container'>
        <div className="row">
            <div className="col">
                <Link to="/agregar" className="btn btn-success mt-2 mt.2"><i className="fa-solid fa-pen-to-square"></i> Agregar Nuevo Producto</Link>
                <table className="table">
                    <thead className="tableTheadBg">
                        <tr>
                            <th>Nombre</th>
                            <th>Categotia</th>
                            <th>Precio</th>
                            <th>Presentación</th>
                            <th>Cantidad</th>
                            <th>Precio Venta</th>
                        </tr>
                    </thead>
                    <tbody>
                        {productos.map((producto, index) => (
                            <tr key = {index}>
                            <td>{producto.nombre}</td>
                            <td>{producto.categoria}</td>
                            <td>{producto.precio}</td>
                            <td>{producto.presentacion}</td>
                            <td>{producto.cantidad}</td>
                            <td>{producto.precio_venta}</td>
                            <td>
                            <Link to ={`/editar/${producto._id}`} className="btn btn-success"><i className="fa-solid fa-pen-to-square"></i> Editar</Link>
                            <button onClick={()=>deleteProducto(producto._id)}className='btn btn-danger'><i class="fa-solid fa-trash-can"></i> Eliminar</button>
                            </td>
                            </tr>
                        ))}
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
)







}
export default CompoMProducto;
